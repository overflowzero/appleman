# Appleman
 This game was created in 2006 (VB 6.0). 
 Then  was ported to Vb.Net NET Framework in 2008-2009. Mostly The code was upgraded with tool.                         
 In 2012 Fixed some bugs to work with mono and added small security against cheat engine
 in 2016 published. **As it was student code. So it lacks core architecture and performance issues.
 And need refactorings**.
### Map Editor
simple map editor program to create maps for the game "Appleman"
### Graphics
While moving to Net FrameWork gdi replaced by Net Framework graphics which rely on gdiplus. Performance decreased due to bad graphic library
### Version
3 
### Run
folder: Appleman/appleman/bin/

You can run on Windows
or with mono (not desired)
```
mono APPLEMANx3.exe
``` 

###Current State
     Refactored a bit with tools
 



