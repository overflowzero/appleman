' Created by Abdurrauf
Imports Microsoft.VisualBasic
Imports System.IO
Public Class appleman
    Private drect As Rectangle
    Private esl_y As Integer

    Public sx(10) As Short
    Dim img As Imaging.ImageAttributes = New Imaging.ImageAttributes
    Dim arrlay() As Byte
    Private bm As Bitmap
    Private a As Byte
    Public m As Byte

    Const tileh As Short = 34
    Public Sub New()

        'MsgBox(Application.StartupPath)

    End Sub

    Public Sub New(ByVal b As Bitmap)
        sx(1) = 2
        sx(2) = 22
        sx(3) = 42
        sx(4) = 22
        sx(5) = 62
        sx(6) = 102
        sx(7) = 122
        sx(8) = 142
        sx(9) = 122

        m = 1

        bm = b
        img.SetColorKey(Color.FromArgb(1, 1, 1), Color.FromArgb(1, 1, 1))
        drect.Width = 20
        drect.Height = 32

    End Sub

    Public Sub draw(ByVal g As Graphics)

        drect.Y = esl_y ' - tileh
        g.DrawImage(bm, drect, sx(m), 2, 20, 32, GraphicsUnit.Pixel, img)

    End Sub

    Public Property x() As Integer
        Get
            Return drect.X
        End Get
        Set(ByVal Value As Integer)

            drect.X = Value
        End Set
    End Property

    Public Property y() As Integer
        Get
            y = esl_y
        End Get
        Set(ByVal Value As Integer)

            esl_y = Value
        End Set
    End Property

End Class

Public Class armo
    Dim a, ason As Byte
    Dim armobit As Bitmap
    Dim img As Imaging.ImageAttributes = New Imaging.ImageAttributes
    Dim refl As Reflection.Assembly = Reflection.Assembly.GetExecutingAssembly()
    Public units(9) As armo_unit
    Public sx(2) As Byte

    Public Const sy As Byte = 0
    Public Const sw As Byte = 12
    Public Const sh As Byte = 12
    Public Sub New()
        For a = 0 To 9
            units(a) = New armo_unit

        Next

        sx(0) = 4
        sx(1) = 15
        sx(2) = 27
        armobit = New Bitmap(refl.GetManifestResourceStream("AppleMANxx.armo.bmp"))
        img.SetColorKey(Color.FromArgb(0, 0, 0), Color.FromArgb(0, 0, 0))
    End Sub

    Public Sub gulle(ByVal istiqamet As Short, ByVal x As Integer, ByVal y As Integer)

        For a = 0 To 9
            If units(a).visible = False Then
                units(a).visible = True
                units(a).loopx = x
                units(a).firstx = x
                units(a).firsty = y
                units(a).drect.Y = y '- 34 'tileh=34

                Select Case istiqamet
                    Case 6

                        units(a).istiqamet = 12
                        units(a).sprite = 0
                    Case -6
                        units(a).istiqamet = -12
                        units(a).sprite = 1
                    Case Else
                        units(a).destroy()
                End Select

                Exit For

            End If
        Next
    End Sub
    Public Sub gulle_loop(ByVal g As Graphics, ByVal blockx As Short, ByVal offset As Short)
        For a = 0 To 9
            If units(a).visible = True Then
                units(a).drect.X = units(a).active() - blockx * 25 - offset  'tilew=25
                g.DrawImage(armobit, units(a).drect, sx(units(a).sprite), sy, sw, sh, GraphicsUnit.Pixel, img)
            End If
        Next

    End Sub
    Public Class armo_unit

        Public istiqamet As Short
        Public drect As Rectangle
        Public firstx, loopx, firsty As Short
        Public sprite As Byte
        Public visible As Boolean
        Public Sub New()
            drect.Width = 12
            drect.Height = 12
            visible = False
            sprite = 2
        End Sub
        Public Function active() As Short

            loopx += istiqamet

            If loopx < firstx - 500 Or loopx > firstx + 750 Then destroy()
            If loopx < 1 Then destroy()
            Return loopx

        End Function
        Public Sub destroy()
            visible = False
            loopx = 0
            sprite = 2
        End Sub

    End Class
End Class

Public Class enemies
    Dim a, arrl As Byte
    Dim enemybit As Bitmap
    Dim img As Imaging.ImageAttributes = New Imaging.ImageAttributes
    Dim refl As Reflection.Assembly = Reflection.Assembly.GetExecutingAssembly()
    Public enemyunit() As enemy_unit
    Public sx(8) As Byte

    Public Const sy As Byte = 0
    Public say As Short
    Const tilew As Byte = 25
    Const tileh As Byte = 34

    Public Const sw As Byte = 23
    Public Const sh As Byte = 37
    Public Sub New(ByVal enemydata() As Integer)

        say = UBound(enemydata)
        ReDim enemyunit(say)
        'MsgBox(say)
        For a = 0 To say
            enemyunit(a) = New enemy_unit

        Next

        For a = 0 To say
            enemyunit(a).n = 4
            enemyunit(a).m = 0
            enemyunit(a).maxjump = 1
            enemyunit(a).firstblockX = CShort(enemydata(a) And &HFFFF)
            enemyunit(a).firstblocky = CShort(enemydata(a) >> 16) '2bayt sagha firlayir chunki short 2bayt-dir

            'MsgBox(enemyunit(a).firstblockX.ToString & "            " & enemyunit(a).firstblocky.ToString & "    " & enemydata(a).ToString)
            enemyunit(a).loopx = enemyunit(a).firstblockX * tilew

            enemyunit(a).loopy = enemyunit(a).firstblocky * tileh

        Next

        'sag
        sx(0) = 0
        sx(1) = 46
        sx(2) = 23
        sx(3) = 46
        'sol
        sx(4) = 72
        sx(5) = 119
        sx(6) = 95
        sx(7) = 119
        'destroy
        sx(8) = 143

        enemybit = New Bitmap(refl.GetManifestResourceStream("AppleMANxx.enemy.bmp"))
        img.SetColorKey(Color.FromArgb(0, 0, 0), Color.FromArgb(0, 0, 0))
    End Sub

    Public Sub enemy_show(ByVal g As Graphics, ByVal blockx As Short, ByVal offset As Short, ByVal arrl As Byte)

        enemyunit(arrl).drect.X = enemyunit(arrl).loopx - blockx * tilew - offset  'tilew=25
        enemyunit(arrl).drect.Y = enemyunit(arrl).loopy ' - tileh
        g.DrawImage(enemybit, enemyunit(arrl).drect, sx(enemyunit(arrl).sprite), sy, sw, sh, GraphicsUnit.Pixel, img)

    End Sub

    Public Class enemy_unit
        Public firstblockX, loopx, firstblocky, loopy, stepx, stepy As Short
        Private mxjmp As Short
        Public visible, activated, inair, lft, rgt, pp As Boolean
        Public sprite As Byte
        Public n, m As Byte

        Public drect As Rectangle

        Public Sub New()
            activated = False
            drect.Width = 23
            drect.Height = 37
            inair = False
            visible = True

            sprite = 3
        End Sub

        Public Property maxjump() As Short
            Get
                Return mxjmp
            End Get
            Set(ByVal value As Short)
                If value >= 0 Then
                    mxjmp = value
                End If
            End Set
        End Property

        Public Sub destroy()
            visible = False
            activated = False
            sprite = 8
        End Sub

    End Class

End Class