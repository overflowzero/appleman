Imports System.IO


Public Class Form1
    Dim mapa(900, 10) As Char
    Dim ss As String
    Const TILEW As Integer = 25
    Const TILEH As Integer = 34

    Dim g, offscrg As Graphics
    Dim b As New Bitmap(TILEW * 900, TILEH * 10)
    Dim picb As Bitmap
    Dim dcr, scrg, dscrg As Rectangle
    Dim sw As Short = 0
    Dim blockx As Short = 0
    Dim k, l As Short
    Dim runs As Boolean
    Dim ppp() As Char = {"1", "2", "3", "4", "5", "6", "7", "8", "9", "*", "0"} '0 to 10====  11 mapunit
    Private Sub Form1_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        runs = False
        End
    End Sub
    Dim filename As String = ""
    Private Sub Form1_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        offscrg = Graphics.FromImage(b)
        HScrollBar1.Maximum = 900 - GroupBox1.Width \ TILEW

        picb = New Bitmap(PictureBox1.BackgroundImage)
        g = Me.GroupBox1.CreateGraphics
        dcr.Height = TILEH
        dcr.Width = TILEW
        'scrg
        scrg.Y = 0
        scrg.Width = GroupBox1.Width
        scrg.Height = GroupBox1.Width
        'dscrg
        dscrg.X = 0
        dscrg.Y = 0
        dscrg.Width = GroupBox1.Width
        dscrg.Height = GroupBox1.Width
        runs = True
        clearr()
        'ishare
       
        Me.Show()
        displayOnScreen()
    End Sub
    Private Sub clearr()
        For l = 0 To 10
            For k = 0 To 900
                mapa(k, l) = "0"
            Next
        Next
        ss = ""
        offscrg.Clear(Color.Black)
        displayOnScreen()
    End Sub

    ''' <summary>
    ''' Just display image on screen
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub displayOnScreen()
        g = Me.GroupBox1.CreateGraphics
        blockx = HScrollBar1.Value
        scrg.X = blockx * TILEW
        g.DrawImage(b, dscrg, scrg, GraphicsUnit.Pixel)

    End Sub



    Private Sub PictureBox1_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles PictureBox1.MouseClick
        sw = e.X - e.X Mod TILEW + 1

    End Sub

    Private Sub GroupBox1_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles GroupBox1.MouseClick
        dcr.X = e.X - e.X Mod TILEW + blockx * TILEW
        dcr.Y = e.Y - e.Y Mod TILEH
        If sw \ TILEW + 1 <= 10 Then

            mapa(dcr.X \ TILEW, dcr.Y \ TILEH) = ppp(sw \ TILEW)
        Else
            mapa(dcr.X \ TILEW, dcr.Y \ TILEH) = "0"
        End If
        offscrg.DrawImage(picb, dcr, sw, 0, TILEW, TILEH, GraphicsUnit.Pixel)
        displayOnScreen()
    End Sub

    Private Sub HScrollBar1_Move(ByVal sender As Object, ByVal e As System.EventArgs) Handles HScrollBar1.Move
        displayOnScreen()

        '  MsgBox(blockx)
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        ss = ""
        'FileSystem.FileOpen(1, "m" & ComboBox1.SelectedItem & ".txt", OpenMode.Output, OpenAccess.Write)

        Dim strfile As String = ""
        If IsNothing(ComboBox1.SelectedItem) Then
            strfile = ""
        Else
            strfile = "m" & ComboBox1.SelectedItem.ToString() & ".txt"
        End If

        If filename.Length > 1 Then strfile = filename
        Using mapwr As New IO.StreamWriter(strfile, False)


            For l = 0 To 10
                ss = ""
                For k = 0 To 900 - 1
                    ss = LTrim(ss & mapa(k, l))
                Next


                mapwr.WriteLine(ss)
            Next
        End Using
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        clearr()
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click

        If OpenFileDialog1.ShowDialog = Windows.Forms.DialogResult.OK Then
            filename = OpenFileDialog1.FileName
            For l = 0 To 10
                For k = 0 To 900
                    mapa(k, l) = "0"
                Next
            Next
            Using mapre As New StreamReader(filename)
                Dim l As Integer
                While mapre.Peek <> -1
                    Dim sr As String = mapre.ReadLine()

                    Dim a() As Char = sr.ToCharArray()
                    Dim kmin As Integer = Math.Min(sr.Length, 900) - 1
                    For i As Integer = 0 To kmin
                        mapa(i, l) = a(i)
                    Next
                    l += 1
                    If l > 10 Then Exit While
                End While



            End Using

            'draw 
            'Cause of our mistake (we placed blank title and enemy last and marked it different
            Dim blanktitlesw As Integer = 10 * TILEW + 1
            Dim enemytitlesw As Integer = 9 * TILEW + 1
            For l = 0 To 10

                For k = 0 To 900 - 1

                    dcr.X = k * TILEW
                    dcr.Y = l * TILEH
                    Select Case mapa(k, l)
                        Case "*"
                            sw = enemytitlesw
                        Case "0"
                            sw = blanktitlesw
                        Case Else
                            Dim v As Integer = blanktitlesw
                            If Integer.TryParse(mapa(k, l), v) = True Then

                                sw = (v - 1) * TILEW + 1
                            End If

                    End Select

                    offscrg.DrawImage(picb, dcr, sw, 0, TILEW, TILEH, GraphicsUnit.Pixel)


                Next

            Next

        End If
        displayOnScreen()
    End Sub

    Private Sub HScrollBar1_Scroll(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ScrollEventArgs) Handles HScrollBar1.Scroll
        displayOnScreen()
    End Sub

    Private Sub GroupBox1_Enter(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles GroupBox1.Enter

    End Sub

    Private Sub Form1_Resize(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Resize
        displayOnScreen()
    End Sub

    Private Sub Form1_Activated(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Activated
        displayOnScreen()
    End Sub
End Class
